FROM alpine:latest
MAINTAINER vincent@siebert.im

RUN apk add --update \
    python \
    py-pip \
    curl \
    bash \
  && pip install awscli \
  && rm -rf /var/cache/apk/*

WORKDIR /app

COPY update-route53.sh .

CMD ["./update-route53.sh"]
